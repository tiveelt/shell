#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <wait.h>
#include "uthash.h"
#include "xssh.h"

#define MAX_LENGTH 1024
#define DELIMS " \t\r\n"

void parseArguments(char *line, char **argv, char *delim);

// Variable
struct var {
	char id[50];	
	char value[50];
	UT_hash_handle hh;
};

struct var *locvars = NULL;

void addVar(char *varId, char *varValue) {
	struct var *s;
	struct var *r;

	HASH_FIND_STR(locvars, varId, s);  /* id already in the hash? */

	r = malloc(sizeof(struct var));
	strcpy(r->id, varId);
	strcpy(r->value, varValue);
	if (s == NULL) {
		HASH_ADD_STR(locvars, id, r);
	} else {
		HASH_REPLACE_STR(locvars, id, r, s);
	}
}

struct var *findVar(char *varId) {
	struct var *s;
	HASH_FIND_STR(locvars, varId, s);
	return s;
}


int main(int argc, char **argv)
{
	// Child Processes
	pid_t childPID;
	int status;
	int endID;

	// Command
	char line[MAX_LENGTH];
	char *linecpy;
	size_t ln;
	char *cmd;
	char *paths;
	char *path;
	char *command;
	char *cmdArgv[256];
	paths = getenv("PATH");

	// Argument options
	int xflag = 0;
	int dflag = 0;
	int dvalue;
	int c;

	opterr = 0;

	/*Parse Arguments with getopt*/
	while ((c = getopt(argc, argv, "xd:")) != -1) {
		switch(c) {
			case 'x':
				xflag = 1;
				break;
			case 'd':
				dflag = 1;
				dvalue = atoi(optarg);
				break;
			default: /* '?' */
				fprintf(stderr, "Usage: %s [-x] [-d debuglevel]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	while (1) {
		printf(">> ");

		if (!fgets(line, MAX_LENGTH, stdin)) break;
		linecpy = strdup(line);

		ln = strlen(linecpy) - 1;
		// Remove trailing \n from linecpy
		if (linecpy[ln] == '\n') {
			linecpy[ln] = '\0';
		}
		parseArguments(linecpy, cmdArgv, " ");

		// cmd is the first argument
		cmd = cmdArgv[0];

		// Parse and execute command
		if (strlen(cmd) != 0) {

			// Clear errors
			errno = 0;

			if (strcmp(cmd, "exit") == 0) {
				break;
			} else if (strcmp(cmd, "quit") == 0) {
				break;

				// SET a variable
			} else if (strcmp(cmd, "set") == 0) {
				if (cmdArgv[1] == NULL) {
					printf("Enter first argument\n");
					break;
				} else if (cmdArgv[2] == NULL) {
					printf("Enter second argument\n");
					break;
				} else {
					addVar(cmdArgv[1], cmdArgv[2]);
					printf("Set successful\n");
				}

				// SHOW a variable
			} else if (strcmp(cmd, "show") == 0) {
				if (cmdArgv[1] == NULL) {
					printf("Enter first argument\n");
					printf("%s %s\n", cmdArgv[0], cmdArgv[1]);
					continue;

				} else {
					printf("%s\n", findVar(cmdArgv[1])->value);
				}

				// External Command
			} else {

				/*printf("%s", cmdArgv[1]);*/
				if (childPID = fork() == 0) {
					printf("%s/n", cmd);

					path = strtok(paths, ":");
					while (path != NULL) {
						command = malloc(strlen(path) + strlen(cmd) + 2);

						strcat(command, path);
						strcat(command, "/");
						strcat(command, cmd);

						if (access(command, F_OK|X_OK) == 0) {
							parseArguments(linecpy, cmdArgv, " ");
							execvp(command, cmdArgv);
						}
						path = strtok(NULL, ":");	
					}
				}

				// Wait until child process ends until continuing parent while loop
				while (childPID = waitpid(-1, NULL, 0)) {
					if (errno == ECHILD) {
						break;
					}
				}
			}
		}
	}
	return 0;
}


void parseArguments(char *line, char **argv, char *delim) {
	int i = 0;
	argv[i] = strtok(line, delim);
	while (argv[i] != NULL) {
		argv[++i] = strtok(NULL, delim);
	}
}
