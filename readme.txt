Name:

How long did the lab take?
~13 hours.

Does your lab work as expected?
Yes except for stdin/stdout.

Are there any issues?
Stdin/stdout. 

What design choices did you make and why?
I chose to create a function called shelly that took a line and executed
commands. This way, I could use shelly in the while loop as well as line
by line for file reading.

