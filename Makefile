CC = gcc
CFLAGS = -Wall -g
COMPILE = $(CC) $(CFLAGS) -c
OBJECTS = xssh.o

all: xssh

xssh: $(OBJECTS)
	$(CC) $(OBJECTS) -o xssh

clean:
	rm -f xssh *.o
