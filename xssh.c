#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <wait.h>
#include "uthash.h"
#include "xssh.h"

#define MAX_LENGTH 1024
#define DELIMS " \t\r\n"

void parseArguments(char *line, char **argv, char *delim, int replace);
void interruptHandler(int SIG); 

// Variable
struct var {
	char id[50];	
	char value[50];
	UT_hash_handle hh;
};

struct var *locvars = NULL;

void addVar(char *varId, char *varValue) {
	struct var *s;
	struct var *r;

	HASH_FIND_STR(locvars, varId, s);  /* id already in the hash? */

	r = malloc(sizeof(struct var));
	strcpy(r->id, varId);
	strcpy(r->value, varValue);
	if (s == NULL) {
		HASH_ADD_STR(locvars, id, r);
	} else {
		HASH_REPLACE_STR(locvars, id, r, s);
	}
}

void rmVar(char *varId) {
	struct var *s;

	// Check for existence of that id
	HASH_FIND_STR(locvars, varId, s); 

	if (s != NULL) {
		HASH_DEL(locvars, s);
		free(s);
	}
}

struct var *findVar(char *varId) {
	struct var *s;
	HASH_FIND_STR(locvars, varId, s);

	if (s == NULL) {
		s = malloc(sizeof(struct var));
		strcpy(s->id, varId);
		strcpy(s->value, varId);
	}
	return s;
}


int main(int argc, char **argv)
{
	// Child Processes
	pid_t childPID;
	int status;
	int endID;

	// Command
	char line[MAX_LENGTH];
	char *linecpy;
	size_t ln;
	char *cmd;
	char *paths;
	char *path;
	char *command;
	char *cmdArgv[256];
	paths = getenv("PATH");

	// Argument options
	int xflag = 0;
	int dflag = 0;
	int dvalue = 1;
	int c;

	opterr = 0;

	/*Parse Arguments with getopt*/
	while ((c = getopt(argc, argv, "xd:")) != -1) {
		switch(c) {
			case 'x':
				xflag = 1;
				break;
			case 'd':
				dflag = 1;
				dvalue = atoi(optarg);
				break;
			default: 
				fprintf(stderr, "Usage: %s [-x] [-d debuglevel]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}
	
	int shelly(char *line) {
		linecpy = strdup(line);
		ln = strlen(linecpy) - 1;

		// Remove trailing \n from linecpy
		if (linecpy[ln] == '\n') {
			linecpy[ln] = '\0';
		}
		parseArguments(linecpy, cmdArgv, " ", 1);

		// cmd is the first argument
		cmd = cmdArgv[0];

		// Parse and execute command
		if (strlen(cmd) != 0) {

			// Clear errors
			errno = 0;
			if (strcmp(cmd, "exit") == 0) {
				if (cmdArgv[1] != NULL) {
					exit(atoi(cmdArgv[1]));
				} else {
					exit(0);
				}
			} else if (strcmp(cmd, "quit") == 0) {
				return -1;
			
			// Change directory
			} else if (strcmp(cmd, "chdir") == 0) {
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {
						printf("Enter directory\n");
					}
					return 0;
				} else if (cmdArgv[2] != NULL) {
					if (dvalue == 2) {
						printf("Too many arguments given\n");	
						return 0;
					}
				} else {
					if (0 != access(cmdArgv[1], F_OK)) {
						if (ENOENT == errno) {
							// does not exist
							printf("Enter valid directory\n");
							return 0;
						} else {
							chdir(cmdArgv[1]);
						}
					}
				}

			// SET a variable
			} else if (strcmp(cmd, "set") == 0) {
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {
						printf("Enter first argument\n");
					}
					return 0;
				} else if (cmdArgv[2] == NULL) {
					if (dvalue == 1) {
						printf("Enter second argument\n");
					}
					return 0;
				} else if (cmdArgv[3] != NULL) {
					if (dvalue == 1) {
						printf("Extra argument detected\n");
					}
					return 0;
				} else {
					addVar(cmdArgv[1], cmdArgv[2]);
					if (dvalue == 1) {
						printf("Set successful\n");
					}
				}

			// SHOW a variable
			} else if (strcmp(cmd, "show") == 0) {
				linecpy = strdup(line);

				// Remove trailing \n from linecpy
				if (linecpy[ln] == '\n') {
					linecpy[ln] = '\0';
				}
				parseArguments(linecpy, cmdArgv, " ", 0);
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {	
						printf("Enter first argument\n");
					}
					return 0;
				}
				int i = 1;
				while (cmdArgv[i] != NULL) {
					if (cmdArgv[i][0] != '$') {
						if (dvalue == 1) {
							printf("%s\n", cmdArgv[i]);
							printf("Enter a variable --beginning with $\n");
						}
						return 0;
					} else {
						char *chopped = cmdArgv[i] + 1;
						printf("%s\n", findVar(chopped)->value);
					}
					i++;

				} 
			} else if (strcmp(cmd, "unset") == 0) {
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {
						printf("Enter variable to unset\n");
					}
					return 0;
				} else {
					rmVar(cmdArgv[1]);

				} 
			// Export command
			} else if (strcmp(cmd, "export") == 0) {
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {
						printf("Enter variable to export\n");	
					}
					return 0;
				} else if (cmdArgv[2] == NULL) {
					if (dvalue == 1) {
						printf("Enter value of the exported variable\n");
					}
					return 0;
				} else {
					char *putStr;
					putStr = malloc(strlen(cmdArgv[1]) + strlen(cmdArgv[2]) + 1);
					strcat(putStr, cmdArgv[1]);
					strcat(putStr, "=");
					strcat(putStr, cmdArgv[2]);

					if (-1 == putenv(putStr)) {
						if (dvalue == 1) {
							printf("Export failed\n");
						}
					}
				}	
			} else if (strcmp(cmd, "unexport") == 0) {
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {
						printf("Enter variable to unexport\n");	
					}
					return 0;
				} else {
					if (-1 == unsetenv(cmdArgv[1])) {
						if (dvalue == 1) {
							printf("Unexport failed\n");
						}
					}
				}
			} else if (strcmp(cmd, "showenv") == 0) {
				if (cmdArgv[1] == NULL) {
					if (dvalue == 1) {
						printf("Enter variable to unexport\n");	
					}
					return 0;
				} else {
					char *testEnv = getenv(cmdArgv[1]);
					if (testEnv != NULL) {
						printf("%s exists\n", testEnv);
					} else {
						printf("Environment variable does not exist\n");
					}
				}

			// External Command
			} else if (strlen(cmd) != 0) {
				
								
				linecpy = strdup(line);

				// Remove trailing \n from linecpy
				if (linecpy[ln] == '\n') {
					linecpy[ln] = '\0';
				}

				int stdinStdout = 0;

				parseArguments(linecpy, cmdArgv, " ", 1);
				// stdin/stdout redirection
				if (cmdArgv[0] != "<" || cmdArgv[0] != ">") {
					int i = 1;
					while (cmdArgv[i] != NULL) {
						if (cmdArgv[i] == "<" || cmdArgv[i] == ">") {
							system(line);
							return 0;
						}
						i++;
					}
				}
				if (childPID = fork() == 0) {
					signal(SIGINT, interruptHandler);
					path = malloc(256);
					path = strtok(paths, ":");
					while (path != NULL) {
						command = malloc(strlen(path) + strlen(cmd) + 2);
						strcat(command, path);
						strcat(command, "/");
						strcat(command, cmd);
						if (access(command, F_OK|X_OK) == 0) {

							linecpy = strdup(line);

							// Remove trailing \n from linecpy
							if (linecpy[ln] == '\n') {
								linecpy[ln] = '\0';
							}

							parseArguments(linecpy, cmdArgv, " ", 1);
								
							// If -x option is set, show substituted command
							if (xflag) {
								char *preview = malloc(256);
								int x = 0;
								while (cmdArgv[x] != NULL) {
									if (x > 0) {
										strcat(preview, " ");
									}
									strcat(preview, cmdArgv[x]);
									x++;
								}		
								printf("%s\n", preview);
							}
							execvp(command, cmdArgv);
						}
						path = strtok(NULL, ":");	
					}
					printf("Invalid command\n");
					raise(SIGTERM);
				}

				// Wait until child process ends until continuing parent while loop
				char childRC[10];
				while (childPID = waitpid(-1, &status, 0)) {
					if (errno == ECHILD) {
						
						// Set variable $? to return code of child
						sprintf(childRC, "%d", status);
						addVar("?", childRC);
						break;
					}
				}
			}
		}
		return 0;

	}
	
	
	// File input
	if (optind < argc) {
		char *fileArgv[256];
		char fileVar[2];
		int i = 0;
		do
		{
			fileArgv[i] = argv[optind];

			// Set vars from file args
			if (i > 0) {
				sprintf(fileVar, "%d", i);
				addVar(fileVar, fileArgv[i]);
			}
			++i;
		}
		while ( ++optind < argc);

		FILE * fp;
		char * line = NULL;
		size_t len = 0;
		ssize_t read;

		fp = fopen(fileArgv[0], "r");
		if (fp == NULL) {
			exit(EXIT_FAILURE);
		}
		
		while ((read = getline(&line, &len, fp)) != -1) {

			// Comment handling
			char *stripped = malloc(256);
			for (i = 0; i < strlen(line); i++) {
				if (line[i] == '#') {
					if (i == 0) {
						stripped[i] = '\n';
					} else {
						stripped[i] = '\0';
					}
					break;
				} else {
					stripped[i] = line[i];
				}
			}
			if (stripped[0] == '\n') {
				continue;
			}
			shelly(stripped);
		}

		if (line) {
			free(line);
		}
		exit(EXIT_SUCCESS);
	}
	
	// Ignore signal in shell
	signal(SIGINT, SIG_IGN);
	
	char pidStr[100];
	sprintf(pidStr, "%ld", (long) getpid());
	addVar("$", pidStr);

	// The main bomdiggity
	while (1) {
		printf(">> ");
		if (!fgets(line, MAX_LENGTH, stdin)) {
			break;
		}
		if (strlen(line) == 1 && line[0] == '\n') {
			continue;
		}
		if (-1 == shelly(line)) {
			break;
		} else {
			continue;
		}
	}
	return 0;
}

// Ctrl-c SIGINT handler
void interruptHandler(int SIG) {
	exit(0);
}

char *replaceArg(char *arg) {
	char *result;
	char *chopped;
	result = malloc(256);
	if (arg == NULL) {
		return arg;
	}
	if (arg[0] == '$') {
		chopped = malloc(strlen(arg) -1);
		chopped = arg + 1;
		result = findVar(chopped)->value;
		if (findVar(chopped)->id == findVar(chopped)->value) {
			return arg;		
		} else {
			return result;
		}
	} else {
		return arg;
	}
}

void parseArguments(char *line, char **argv, char *delim, int replace) {
	int i = 0;
	argv[i] = strtok(line, delim);
	while (argv[i] != NULL) {
		++i;
		if (replace) {
			argv[i] = replaceArg(strtok(NULL, delim));
		} else {
			argv[i] = strtok(NULL, delim);
		}
	}
}
